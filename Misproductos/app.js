var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var productosRouter = require('./routes/productos');
var tokenRouter = require('./routes/token');
var Usuario = require('./models/usuario');
var Token = require('./models/Token');
var productosApiRouter = require('./routes/api/ProductoApiRouter');
var usuariosApiRouter = require('./routes/api/UsuarioApiRouter');
var authApiRouter = require('./routes/api/AuthApiRouter');
const store = new session.MemoryStore;
var app = express();


app.set('secretKey', '123456789a');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'meganet'
}));

var mongoose = require('mongoose');

var mongoDB = 'mongodb://localhost/Misproductos';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection error:'));



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.get('/login',function(req, res){
  res.render('session/login');
});
app.post('/login',function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){

    if(err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.login(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/productos');
    });

  })(req, res, next);
});
app.get('/logout',function(req, res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword',function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword',function(req, res, next){

  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(!usuario) return   res.render('session/forgotPassword', {info: {message: 'no se encontro el email'}});
  
    Usuario.resetPassword(function(err){
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

  });
  res.render('session/forgotPasswordMessage');
});

app.get('/resetPassword/:token',function(req, res, next){
    Token.findOne({token: req.params.token}, function(err, token){
      if(token) return res.status(400).send({type: 'not-verified', msg: 'No existe ese token'});
      Usuario.findById(token._userid, function(err, usuario){
        if(!usuario) return  res.status(400).send({type: 'not-verified', msg: 'No existe el usuario '});
        res.render('session/resetPassword', {errors: {}, usuario: usuario});
      });

    });
  
});


app.post('/resetPassword/',function(req, res, next){

    if(req.body.password != req.body.confirm_passqord) {
      res.render('session/resetPassword', {errors: {confirm_passqord: {message: 'No coindicen los passwords'}}});
      return ;
    } 
    Usuario.findOne({email: req.body.email}, function(err, usuario){
      Usuario.password = req.body.password;
      Usuario.save(function(err){
        if(err) {

        }else{
          res.redirect('/login');
        }
      });
 
 
    });



});



app.use('/usuarios', usersRouter);

//seguridad de la pagina si el usuario no esta logueado
app.use('/productos',loggedIn, productosRouter);
app.use('/token', tokenRouter);

app.use('/api/auth', authApiRouter);
app.use('/api/productos',validarUsuario, productosApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


 function validarUsuario(req, res, next){
   jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
        res.json({status: 'error', message: err.message, data: null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verificado: '+decoded);
      next(); 
    }
   });

 }

function loggedIn(req, res, next){

    if(req.user){
      next();
    }else{
      console.log('user sin loguearse');
      res.redirect('/login');
    }
};
module.exports = app;
