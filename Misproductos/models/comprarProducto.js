var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var compraSchema = new Schema({
    fechacreacion: Date,
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'},
    producto: { type: mongoose.Schema.Types.ObjectId, ref: 'Producto'},
    preciot: Number
});


compraSchema.methods.Preciocompra = function(){
    return moment(this.fechacreacion) + 1;
}

module.exports = mongoose.model('Comprar',compraSchema );