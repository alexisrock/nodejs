var mongooose = require('mongoose');
var Schema = mongooose.Schema;

let productoSchema = new Schema({
    code: Number,
    nombre: String,
    precio: Number,
    cantidad: Number,
    marca: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere',  sparce: true}
    }
});

productoSchema.statics.createInstance = function(code, nombre, precio, cantidad, marca, ubicacion){
    return new this({
        code: code,
        nombre: nombre,
        precio: precio,
        cantidad: cantidad,
        marca: marca
     
    });
   }


productoSchema.methods.toString = function(){
    return 'code: '+this.code+ '| nombre: '+this.nombre+'| cantidad: '+this.cantidad;
}

productoSchema.statics.allproductos = function(pr){
 return this.find({}, pr);
}

productoSchema.statics.add = function(aproduc, pr){
    this.create(aproduc, pr);
}







// var Producto = function(code, nombre, precio, cantidad, marca, ubicacion){
// this.code = code;
// this.cantidad = cantidad;
// this.marca = marca;
// this.nombre = nombre;
// this.precio = precio;
// this.ubicacion = ubicacion;

// }


module.exports = mongooose.model('Producto', productoSchema);


// Producto.allproductos =[];
// Producto.add = function(productoa){
//     Producto.allproductos.push(productoa);
// }

// Producto.findById = function(productid) {

//  var produc = Producto.allproductos.find(x=> x.id == productid )
//     if (produc) {
//         return produc;
//     } else{
//         throw  new Error(`No se encuestra producto con el id: ${productid}`)
//     }
// }

// Producto.removeByID = function(productid){

//     for(var i = 0; i <  Producto.allproductos.length ; i++ ){
//         if (Producto.allproductos[i].id==productid) {
//             Producto.allproductos.splice(i,1);
//             break;
//         }
//     }
    
// }

// var a = new Producto(1, 'Teclado', 60000, 6, 'genius');
// var b = new Producto(2, 'Mouse', 40000, 6, 'genius');
// var c = new Producto(3, 'Parlantes', 90000, 6, 'genius');

// // Producto.add(a);
// // Producto.add(b);
// // Producto.add(c);

// module.exports =  Producto;