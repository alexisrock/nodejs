var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var tokenSchema = new Schema({
    _userid: { type: mongoose.Schema.Types.ObjectId, required: true,  ref: 'Usuario'},
    token: {
        type: String,
        required: [true, 'Se requiere un token']        
         },
    fechacreacion: {type: Date, required:true, default: Date.now, expires: 43200 } //esto sirve para dentro de la base de datos expire el registro despues de cierto tiempo lo elimina
});