var mongoose = require('mongoose');
var compra = require('./comprarProducto');
var bcrypt = require('bcrypt');
const crypto = require('crypto');
var Schema = mongoose.Schema;
const Token = require('../models/Token');
const mailer = require('../Mailer/mailer');
var uniqueValidator = require('mongoose-unique-validator');

const saltRounds = 10;


const validateEmail = function(email){
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}





//creando modelos con caracteristicas de requerido y funciones

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        unique: true, //mongoose-unique-validator
        lowercase: true
      //  validate: [validateEmail, 'Ingrese un email valido']
 

    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio es obligatorio'],
     
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});



//funcion que se ejecuta antes de HACER un save
usuarioSchema.pre('save', function(next){
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
     next();
});

usuarioSchema.methods.validPassword = function(password){    
    var result = bcrypt.compareSync(password, this.password);
    console.log("result de validacion "+result);
    return  result;
}


usuarioSchema.methods.comprar = function(producid, precioTotal, us){
    var comprar = new compra({usuario: this._id, producto: producid, preciot: precioTotal});
    console.log(comprar);
    comprar.save(us);
}

usuarioSchema.methods.enviar_email_bienvenida = function(us) {
    // const token = new Token({_userid: this.id, token: crypto.randomBytes(16).toString('hex')});
    // const email_destination = this.email;
    // token.save(function(err){
    //     if (err) {return console.log(err.message); }
    //     const mailOptions = {
    //         from: 'no-reply@meganet.com.co',
    //         to: email_destination,
    //         subject: 'verificacion de cuenta',
    //         text: 'hola, \n\n'+'por favor verificar la cuenta haga click en este enlace '+'http://localhost:3000'+'\/token/confirmation\/'+token.token+'.\n'
    //     };
    //     mailer.sendMail(mailOptions, function(err){
    //         if (err) {return console.log(err.message); }
    //         console.log('a verification email has been sent to '+email_destination);
    //     });


    // });
 
}

usuarioSchema.methods.resetPassword = function(pr){
    const token = new Token({_userid: this._id, token: crypto.randomBytes(16).toString('hex')});

    // const email_destination = this.email;
    // token.save(function(err){
    //     if (err) {return console.log(err.message); }
    //     const mailOptions = {
    //         from: 'no-reply@meganet.com.co',
    //         to: email_destination,
    //         subject: 'verificacion de cuenta',
    //         text: 'hola, \n\n'+'por favor verificar la cuenta haga click en este enlace '+'http://localhost:3000'+'\/token/confirmation\/'+token.token+'.\n'
    //     };
    //     mailer.sendMail(mailOptions, function(err){
    //         if (err) {return console.log(err.message); }
    //         console.log('a verification email has been sent to '+email_destination);
    //     });


    // });

}







module.exports = mongoose.model('Usuario',usuarioSchema);
