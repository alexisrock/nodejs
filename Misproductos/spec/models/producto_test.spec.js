
var mongoose = require('mongoose');
var Producto = require('../../models/producto');

describe('testeando productos',function(){
    beforeEach(function(done){
        mongoose.disconnect();

    var mongoDB =  'mongodb://localhost/testdb';
    mongoose.connect(mongoDB,  { useNewUrlParser: true, useUnifiedTopology: true,useCreateIndex: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB Connection error:'));
    db.once('open',function(){
        console.log('Conectando a la base de datos prueba');
        done();
    });
  });
    afterEach(function(done){
        Producto.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
        
    });

    afterAll((done) => { mongoose.connection.close(done) });

    describe('Producto.add',function() {
        it('agregar un producto', function(done){
            var producta = new Producto({code: 1, nombre: 'Mouse', precio: 0000, cantidad: 8, marca: 'genius'});1
            Producto.add(producta, function(err, newproduc){
                if(err) console.log(err);
                Producto.allproductos(function(err, produc){
                    expect(produc.length).toEqual(1);
                    expect(produc[0].code).toEqual(producta.code);
                    done();
                });
            
            });

        });
    
    });




describe('Producto.list',function() {
    it('Crear una instancia de producto', function(done){
        Producto.allproductos(function(err, produc){
                expect(produc.length).toBe(0);
                done();
            });
        });

    });

describe('Producto.add',function() {
        it('agregar un producto', function(done){
            var producta = new Producto({code: 2, nombre: 'Teclado', precio: 70000, cantidad: 8, marca: 'genius'});
            Producto.add(producta, function(err, newproduc){
                if(err) console.log(err);
                Producto.allproductos(function(err, produc){
                    expect(produc.length).toEqual(1);
                    expect(produc[0].code).toEqual(producta.code);
                    done();
                });
            
            });

        });
    
    });
    describe('Producto.Findbycode',function() {
        it('debe devolver producto con el code', function(done){
            Producto.allproductos(function(err, produc){
                    expect(produc.length).toBe(0);
                    
                    var aproduc = new Producto({code: 2, nombre: 'Teclado', precio: 70000, cantidad: 8, marca: 'genius'});
                    Producto.add(aproduc, function(err, newproduc){
                        if(err) console.log(err);
                    
                        var aproduc2 = new Producto({code: 3, nombre: 'mouse', precio: 90000, cantidad: 8, marca: 'genius'}); 
                        Producto.add(aproduc2, function(err, newprodu){
                            if(err) console.log(err);
                            Producto.findByCode(2, function(error, targetproduc){
                                expect(targetproduc.code).toBe(aproduc.code);
                                expect(targetproduc.nombre).toBe(aproduc.nombre);
                                done();
                            });

                        });
                    });

                });
            });
    
        });
    



    
});




// describe('testeando...',()=> {
//     console.log('testeando...');
// });
// describe('Producto.allproductos',()=> {
//     it('Comienza vacia'), ()=>{
//         expect(producto.allproductos.length).toBe(0);
//     }

// });


// describe('Producto.add',()=> {
//     it('agregar producto'), ()=>{
//         expect(producto.allproductos.length).toBe(0);
//         var a = new Producto(1, 'Parlantes', 70000, 6, 'genius');
//         producto.add(a);
//         expect(producto.allproductos.length).toBe(1);
//         expect(producto.allproductos[0].length).toBe(a);
//     }

// });

// describe('Producto.findById',()=> {
//     it('Debe devolver producto 1'), ()=>{
//         expect(producto.allproductos.length).toBe(0);
//         var aproduc = new Producto(7, 'parlante', 80000, 6, 'genius');
//         var bproduc = new Producto(8, 'parlante', 80000, 6, 'glogitech');
//         Producto.add(aproduc);
//         Producto.add(bproduc);
//         var target = producto.findById(7);
//         expect(target.id).toBe(7);
//         expect(target.nombre).toBe(aproduc.nombre);
//     }

// });