var mongoose = require('mongoose');
var Producto = require('../../models/producto');
var Usuario = require('../../models/usuario');
var Comprar = require('../../models/comprarProducto');
const { deleteMany } = require('../../models/producto');

describe('testeando usuarios',function(){

    beforeEach(function(done){
        var mongoDB =  'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,  { useNewUrlParser: true, useUnifiedTopology: true,useCreateIndex: true});
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB Connection error:'));
        db.once('open',function(){
            console.log('Conectando a la base de datos prueba');
            done();
        });
      });
        afterEach(function(done){
            Comprar.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Usuario.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    Producto.deleteMany({}, function(err, success){
                        if(err) console.log(err);
                        done();
                    });
                });

            });            
        });

        describe('cuando un usuario compre un producto',function() {
            it('comprar producto', function(done){
                const usuario = new Usuario({ nombre: 'Alexis'});
                usuario.save();

                const producto = new Producto({ code: 1, nombre: 'Teclado', precio: 60000, cantidad:6, marca:'genius'});
                producto.save();

                    usuario.comprar(producto.id, 150000,function(){

                    });



                });
        
            });



});