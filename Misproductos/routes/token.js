var express = require('express');
var router = express.Router();
var tokenController = require('../Controllers/TokenController');

router.get('/confirmation/:token', tokenController.confirmationGet);
module.exports = router;