var express = require('express');
var router = express.Router();
var ProductoController = require('../../Controllers/api/ProductoControllerApi');

router.get('/', ProductoController.producto_list);
// router.get('/create', ProductoController.producto_create_get);
router.post('/create', ProductoController.producto_create_post);
router.delete('/delete', ProductoController.producto_delete_post);
// router.get('/:id/update', ProductoController.producto_update_get);
 router.put('/:id/update', ProductoController.producto_update_post);

module.exports = router;