var express = require('express');
var router = express.Router();

const AuthController = require('../../Controllers/api/AuthController');

router.post('/authenticate',AuthController.authenticate );
router.post('/forgotPassword',AuthController.forgotPassword );
module.exports = router;