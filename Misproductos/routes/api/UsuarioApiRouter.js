var express = require('express');
var router = express.Router();
var UsuarioController = require('../../Controllers/api/UsuarioControllerApi');

router.get('/', UsuarioController.usuarios_list);
// router.get('/create', ProductoController.producto_create_get);
router.post('/create', UsuarioController.usuarios_create);

module.exports = router;