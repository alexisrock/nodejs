var Usuario = require('../models/usuario');
var Token = require('../models/Token');

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({token: req.params.token}, function(err, token){
            if(!token){ return res.status(400).send({ type: 'not-verifed', msg: 'no se pudo verificar la cuenta '})}
            Token.findById(token._userid, function(err, usuario){
                if(!usuario){ return res.status(400).send({ type: 'not-verifed', msg: 'no se encontro un usuario con ese id '})}
                if(!usuario.verificado){ return res.redirect('/usuarios');             }
                usuario.verificado = true;
                usuario.save(function(err){
                    if(!usuario){ return res.status(400).send({   msg: err.message})}
                    res.redirect('/');


                });
           

            });
        });
    }
    
}