const Usuario = require('../../models/usuario');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
    authenticate: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, userinfo){
            if(err){
                next(err);                
            }else{
                if(userinfo===null){ return   res.status(401).json({status: 'error', message: 'Usuario no encontrado'});}
                if(userinfo!= null && bcrypt.compareSync(req.body.password, userinfo.password)){
                    userinfo.save(function(err, usuario){
                        const token = jwt.sign({id: userinfo._id}, req.app.get('secretKey'), { expiresIn: '7d'});
                        res.status(200).json({ message: 'Usuario encontrado', data: {usuario: usuario, token:token}});
                    });
                }else{
                    res.status(401).json({status: 'error', message: 'Invalid email/password'});
                }


            }
        });
    },
    forgotPassword: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, usuario){
            if(!usuario) return   res.status(401).json({ message: 'No existe el usuario'});
            Usuario.resetPassword(function(err){
                if(err){ return next(err);}
                res.status(200).json({ message: 'Se envio un email con la contraseña'});
            });
        });
    }


}
