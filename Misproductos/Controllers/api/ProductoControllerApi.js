var Producto = require('../../models/producto');

exports.producto_list = function(req, res){
    Producto.allproductos().exec((err, producs)=>{   
        res.status(200).json({producs});  
    });



   }
   
exports.producto_create_post = function(req, res){
    var product = new Producto(req.body.id, req.body.nombre, req.body.precio, 
            req.body.cantidad, req.body.marca);
    console.log(product);
    Producto.add(product);
    res.status(200).json({
        productos:  product
    }); 
}


exports.producto_delete_post = function(req, res){
    Producto.removeByID(req.body.id);
    res.status(204).send();
   }
   

   exports.producto_update_post = function(req, res){
    var product = Producto.findById(req.params.id);
    product.id = req.body.id;
    product.nombre = req.body.nombre;
    product.precio = req.body.precio;
    product.cantidad = req.body.cantidad;
    product.marca = req.body.marca;  
    res.status(200).json({
        productos:  product
    }); 
}