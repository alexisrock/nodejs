var Producto = require('../models/producto');


exports.producto_list = function(req, res){

    Producto.allproductos().exec((err, producs)=>{
        res.render('Producto/index', {producs});
    });

}

exports.producto_create_get = function(req, res){
    res.render('Producto/create');
}

exports.producto_create_post = function(req, res){
 
    let product = new Producto({
        code: req.body.id,
        nombre: req.body.nombre, 
        precio: req.body.precio, 
        cantidad: req.body.cantidad, 
        marca: req.body.marca});
console.log("datos: "+product);
    Producto.add(product);
    res.redirect('/Productos');
}

exports.producto_delete_post = function(req, res, next){
 Producto.findByIdAndDelete(req.body.id, function(err){
    if (err) {
        next(err);
    }
    else{
        res.redirect('/Productos');
    }
 });

}


exports.producto_update_get = function(req, res){
   
    let product = Producto.findById(req.params.id);
    console.log("producto: "+product);
    console.log("producto nombre: "+product.nombre);
    res.render('Producto/update', {product});
}

exports.producto_update_post = function(req, res){
    var product = Producto.findById(req.params.id);
    product.id = req.body.id;
    product.nombre = req.body.nombre;
    product.precio = req.body.precio;
    product.cantidad = req.body.cantidad;
    product.marca = req.body.marca;  
    res.redirect('/Productos');
}