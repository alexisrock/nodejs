const  passport = require('passport');
const   localStrategy = require('passport-local').Strategy;
const  Usuario = require('../models/usuario');

passport.use(new localStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, usuar){
            if(err) return done(err);
            if(!usuar) return done(null, false, {message: 'Email no existe'});
            if(!usuar.validPassword(password)){
                console.log("usuario a validar"+usuar);
                console.log("password a validar"+password);
                return done(null, false, {message: 'password incorrecto'});
            }
             

            return done(null, usuar);
        });
    }

));

passport.serializeUser(function(user, us){
    us(null, user.id);

});

passport.deserializeUser(function(id, us){
    Usuario.findById(id, function(err, usuario){
        us(err, usuario);
    });
});

module.exports = passport;